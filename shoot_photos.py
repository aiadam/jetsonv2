from jetson_utils import videoSource, videoOutput, cudaToNumpy
from PIL import Image
import time
import os

def set_light_cameras(id, brightness, contrast, gamma, backlight_compensation, sharpness):
    if int(os.popen(f"v4l2-ctl -d '/dev/video{id}' --get-ctrl=brightness").read().split(" ")[-1]) != brightness:
        os.system(f"v4l2-ctl -d '/dev/video{id}' --set-ctrl=brightness={brightness}")

    if int(os.popen(f"v4l2-ctl -d '/dev/video{id}' --get-ctrl=contrast").read().split(" ")[-1]) != contrast:
        os.system(f"v4l2-ctl -d '/dev/video{id}' --set-ctrl=contrast={contrast}")

    if int(os.popen(f"v4l2-ctl -d '/dev/video{id}' --get-ctrl=gamma").read().split(" ")[-1]) != gamma:
        os.system(f"v4l2-ctl -d '/dev/video{id}' --set-ctrl=gamma={gamma}")

    if int(os.popen(f"v4l2-ctl -d '/dev/video{id}' --get-ctrl=backlight_compensation").read().split(" ")[-1]) != backlight_compensation:
        os.system(f"v4l2-ctl -d '/dev/video{id}' --set-ctrl=backlight_compensation={backlight_compensation}")

    if int(os.popen(f"v4l2-ctl -d '/dev/video{id}' --get-ctrl=sharpness").read().split(" ")[-1]) != sharpness:
        os.system(f"v4l2-ctl -d '/dev/video{id}' --set-ctrl=sharpness={sharpness}")

def capture():

    name_product = input("product name: ")

    #if os.path.isdir('images/{}'.format(name_dir)) == False:
    #    os.system("mkdir 'images/{}'".format(name_dir))

    len_dir = len(os.listdir('images/'))
    name_dir = len_dir+1
    os.system("mkdir 'images/{}'".format(name_dir))

    n_loop = 0 

    csi_w = 3264
    csi_h = 2464
    csi_fps = 21

    usb_w = 3264
    usb_h = 2448
    usb_fps = 15

    csi0_source = "csi://0"
    usb1_source = "v4l2:///dev/video1"
    usb2_source = "v4l2:///dev/video2"   

    csi0 = videoSource(csi0_source, options = {'width': csi_w, 'height': csi_h, 'framerate': csi_fps})
    usb1 = videoSource(usb1_source, options = {'width': usb_w, 'height': usb_h, 'framerate': usb_fps})
    usb2 = videoSource(usb2_source, options = {'width': usb_w, 'height': usb_h, 'framerate': usb_fps})

    display0 = videoOutput("display://{}".format(id), argv=['--width={}'.format(csi_w),'--height={}'.format(csi_h)]) 
    display1 = videoOutput("display://{}".format(id), argv=['--width={}'.format(usb_w),'--height={}'.format(usb_h)]) 
    display2 = videoOutput("display://{}".format(id), argv=['--width={}'.format(usb_w),'--height={}'.format(usb_h)]) 

    while True:
        time.sleep(5) #2 -> 5 sekund różnicy między zdjęciami  --> (+3 sekundy)

        img0 = csi0.Capture()
        img1 = usb1.Capture()
        img2 = usb2.Capture()

        print('capture done')

        if img0 is None or img1 is None or img2 is None:
            continue
        
        #display.Render(img)
        #display.SetStatus("Camera {}".format(id))

        img0 = cudaToNumpy(img0)
        img1 = cudaToNumpy(img1)
        img2 = cudaToNumpy(img2)

        img0 = Image.fromarray(img0, "RGB")
        img0.save('images/{}/{}_csi0_{}.jpg'.format(name_dir, name_product, n_loop))

        img1 = Image.fromarray(img1, "RGB")
        img1.save('images/{}/{}_usb1_{}.jpg'.format(name_dir, name_product, n_loop))

        img2 = Image.fromarray(img2, "RGB")
        img2.save('images/{}/{}_usb2_{}.jpg'.format(name_dir, name_product, n_loop))

        #jetson_utils.saveImageRGBA('images/img_camera_{}_{}.jpg'.format('csi', numb), img0)
        #jetson_utils.saveImageRGBA('images/img_camera_{}_{}.jpg'.format('usb1', numb), img1)
        #jetson_utils.saveImageRGBA('images/img_camera_{}_{}.jpg'.format('usb2', numb), img2)
        
        if not display0.IsStreaming() or not display1.IsStreaming() or not display2.IsStreaming():
            break

        # ograniczenie liczby zdjęć
        if n_loop == 3:
            break

        n_loop +=1

if __name__ == "__main__":

    set_light_cameras(1, -23, 20, 80, 2, 3)
    set_light_cameras(2, -23, 20, 80, 2, 3)

    capture()
