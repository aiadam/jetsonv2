from jetson_utils import videoSource, videoOutput
import threading


def capture(cam_source, id, resolution):
    cam = videoSource(cam_source, options = {'width': resolution[0], 'height': resolution[1], 'framerate': resolution[2]})
    display = videoOutput("display://{}".format(id), argv=['--width={}'.format(resolution[0]),'--height={}'.format(resolution[1])]) 

    while True:

        img = cam.Capture()
        if img is None: # capture timeout
            continue
        
        display.Render(img)
        display.SetStatus("Camera {}".format(id))

        if not cam.IsStreaming() or not display.IsStreaming():
            break

if __name__ == "__main__":

    csi0_source = "csi://0"
    usb1_source = "v4l2:///dev/video1"
    usb2_source = "v4l2:///dev/video2"   

    csi_res = [3264, 2464, 21]
    csi_res1 = [1640, 1232, 30]

    usb_res = [3264, 2448, 15]
    usb_res1 = [1640, 1224, 30]

    t1 = threading.Thread(target=capture, args=(csi0_source,0, csi_res1, ))
    t2 = threading.Thread(target=capture, args=(usb1_source,1, usb_res1, ))
    t3 = threading.Thread(target=capture, args=(usb2_source,2, usb_res1, ))

    t1.start()
    t2.start()
    t3.start()

    t1.join()
    t2.join()
    t3.join()